import React,{useState} from 'react'
import StarRatingComponent from 'react-star-rating-component';
import ReactStars from 'react-stars'

const CustomRating = () => {
    const [val, setVal] = useState(2.5)
    const onStarClickHalfStar=(nextValue, prevValue, name, e)=> {
        console.log(e)
      const xPos = (e.pageX - e.currentTarget.getBoundingClientRect().left) / e.currentTarget.offsetWidth;
  
      if (xPos <= 0.5) {
        nextValue -= 0.5;
      }
  
      console.log('name: %s, nextValue: %s, prevValue: %s', name, nextValue, prevValue);
      setVal(nextValue)
    //   // console.log(e);
    //   this.setState({rating_half_star: nextValue});
    }
    const ratingChanged = (newRating, name) => {
        console.log(newRating, name)
      }
    return (
        <div>
            <div style={{fontSize: 40}}>
                <p>&#128540;</p>
          <StarRatingComponent
            name="myapp"
            starColor="#ffb400"
            // emptyStarColor="#efefef"
            value={val}
            onStarClick={onStarClickHalfStar}
            // onStarClick={this.onStarClickHalfStar}
            // renderStarIcon={(index, value) => {
            //   return (
            //     <span>
            //       <i className={index <= value ? 'fas fa-star' : 'far fa-star'} />
            //     </span>
            //   );
            // }}
            renderStarIconHalf={() => {
              return (
                // <p>&#128540;</p>
                // <span>
                //   <span style={{position: 'absolute'}}><i className="fas fa-star-half" /></span>
                //   <span><i className="fas fa-star" /></span>
                // </span>
                <span>
                  <span style={{position: 'absolute'}}><i className="far fa-star" /></span>
                  <span><i className="fas fa-star-half" /></span>
                </span>
              );
            }} />
        </div>
        {/* <div style={{fontSize: "40px"}} */}
        <div style={{fontSize: "100px", display: "flex", justifyContent: "center", marginBottom: "50px"}}>
        <ReactStars count={7} onChange={ratingChanged} size={50} half={true} color2={'#d3d3d3'} />  
        </div>
        </div>
    )
}

export default CustomRating
