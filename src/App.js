import './App.css';
import React,{useState} from 'react';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogout } from 'react-google-login';
import Rating from "./rating";
import CustomRating from './customRating';

function App() {
  const [error, setError] = useState(undefined);
  const [submitting, setSubmitting] = useState(false);

  const responseGoogle = (response) => {
    console.log("google",response);
  }
  const responseFacebook = (response) => {
    console.log("facebook",response);
  }
  const componentClicked = (data) =>{
    console.warn("data",data);
  }
  const logout = (data)=>{
    console.log("google logout", data);
  }
  const clientId="614098796579-5kurfqjql9jaijpeh8td2tto62aq6mpu.apps.googleusercontent.com"
  const appId="1239793053125561"
  const [data,setData] = useState({email:"",password:""  })
  const handleInput=(e)=>{
    const name= e.target.name;
    const value=e.target.value;
    setData({...data, [name] : value})
  }

  const postData = async (e)=>{
    setSubmitting(true)
     e.preventDefault();
     const {email, password} = data;
     console.log(email,password)

     fetch("http://localhost:9000/signup",{
       method:"POST",
       headers: {"Content-Type": "application/json"},
       body: JSON.stringify({
         email, password
       })
     })
     .then((res)=>res.json())
     .then((data)=>{
       if(data.status==='ok'){
        console.log("Success")
       }else{
        setError(data.error)
       }
      }
     )
  }

  const loginData=(e)=>{
    e.preventDefault();
    const {email, password} = data;

    fetch("http://localhost:9000/login",{
      method:"POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        email, password
      })
    })
    .then((resp)=>resp.json())
    .then((data)=>{
      if(data.status==='ok'){
       console.log("Got the Token", data.data)
       console.log("Success")
      }else{
       setError(data.error)
      }
     }
    )
 }
  
  return (
    <div className="App">
      {error}
      <div style={{width: "500px", height: "500px", display: "flex", flexDirection:"column", justifyContent: "center", alignItems:"center"}}>
      <input type="email" placeholder="enter your email" name="email" value={data.email} onChange={handleInput} style={{width: "300px", height: "30px"}} className={submitting && data.email.trim().length===0 ? 'error' : ""} required/><br/><br/>
      <input type="password" placeholder="enter your password" name="password" value={data.password} onChange={handleInput} style={{width: "300px", height: "30px"}} className={submitting && data.password.trim().length===0 ? 'error' : ""} required/><br/><br/>
      <button type="button" style={{width: "300px", height: "30px", backgroundColor: "green", color: "white"}} onClick={postData}>SignUp</button>
      <button type="button" style={{width: "300px", height: "30px", backgroundColor: "green", color: "white"}} onClick={loginData}>Login</button>
      </div>
      <GoogleLogin
    clientId={clientId}
    buttonText="Login With Your Google Account"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />
  <GoogleLogout
      clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
      buttonText="Logout"
      onLogoutSuccess={logout}
    >
    </GoogleLogout>
  <FacebookLogin
    appId={appId}
    autoLoad={false}
    fields="name,email,picture"
    onClick={componentClicked}
    callback={responseFacebook} />,
    <div> Rating
     <Rating/>
    </div>
    <CustomRating/>
  </div>
  );
}

export default App;
