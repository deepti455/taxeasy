module.exports = {
    ci: {
      collect: {
        // settings: {
        //   emulatedFormFactor: 'desktop'
        //   },
        url: [
            'https://logo-maker-web-dev.algo.design.vpsvc.com/',
            'https://logo-maker-web-dev.algo.design.vpsvc.com/wizard/company-name',
            'https://logo-maker-web-dev.algo.design.vpsvc.com/studio/inspire',
          ],
        },
      upload: {
        target: 'temporary-public-storage',
      },
      assert: {
         preset: 'lighthouse:no-pwa',
         assertions: {
        'csp-xss' : 'error',
        'categories:performance' : ['error', {minScore: 0.6}],
        'categories:accessibility' : ['error', {minScore: 0.6}],
        'categories:best-practices' : ['error', {minScore: 0.6}],
        'categories:seo' : ['error', {minScore: 0.6}],
        // "audit": ["warn",{"minLength": 0}]
          // "audit-id-1": ["warn", {"maxNumericValue": 4000}],
          // "audit-id-2": ["warn", {"minScore": 0.8}],
          // "audit-id-3": ["warn", {"minLength": 0}]
             },
        },
    },
  }
  